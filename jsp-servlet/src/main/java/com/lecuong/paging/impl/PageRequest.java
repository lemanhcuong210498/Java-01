package com.lecuong.paging.impl;

import com.lecuong.paging.Pageable;

public class PageRequest implements Pageable {

    private int pageSize;
    private int pageIndex;

    public PageRequest(int pageSize, int pageIndex){
        this.pageSize = pageSize;
        this.pageIndex = pageIndex;
    }

    public static PageRequest of(int pageSize, int pageIndex){
        return new PageRequest(pageSize, pageIndex);
    }

    @Override
    public int getPage() {
        return pageIndex;
    }

    @Override
    public int getSize() {
        return pageSize;
    }

    @Override
    public int getOffset() {
        return (pageIndex - 1)*pageSize + 1;
    }
}
