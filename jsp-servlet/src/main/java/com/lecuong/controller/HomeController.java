package com.lecuong.controller;

import com.lecuong.model.request.Auth;
import com.lecuong.utils.FormUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/home")
public class HomeController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        String username = req.getParameter("username");
//        String password = req.getParameter("password");
//        Auth auth = new Auth();
//        auth.setUserName(username);
//        auth.setPassword(password);

        Auth auth = FormUtil.toModel(Auth.class, req);

        System.out.println(auth.getUserName());
        System.out.println(auth.getPassword());

        RequestDispatcher rd = req.getRequestDispatcher("/views/home.jsp");
        rd.forward(req, resp);

    }
}
