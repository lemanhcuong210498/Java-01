package com.lecuong.repository;

import com.lecuong.entity.User;

import java.sql.SQLException;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByUserNameAndPassword(String userName, String password) throws SQLException, IllegalAccessException, NoSuchFieldException, InstantiationException;

}
