package com.lecuong.utils;

import com.lecuong.common.annotation.Column;
import com.lecuong.common.annotation.Entity;
import com.lecuong.common.annotation.Id;

public class AnnotationUtil {

    public static String getTableName(Class<?> clazz){
        return clazz.getDeclaredAnnotation(Entity.class).name();
    }

    public static String getPrimaryKey(Class<?> tClass, String fieldName) throws NoSuchFieldException {
        return tClass.getDeclaredField(fieldName).getDeclaredAnnotation(Id.class).value();
    }

    public static String getFieldName(Class<?> tClass, String fieldName) throws NoSuchFieldException {
        return tClass.getDeclaredField(fieldName).getDeclaredAnnotation(Column.class).value();
    }

}
