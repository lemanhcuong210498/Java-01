package com.lecuong.utils.pool;

import com.lecuong.utils.MySQLConnectionUtil;
import com.lecuong.utils.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class ConnectionPoolImpl implements ConnectionPool {

    private static ResourceBundle rs = ResourceBundle.getBundle("DatabaseInformation");
    private static final LinkedList<Connection> connectionInUes = new LinkedList<>();
    private static final int MAX_CONNECTION = Integer.parseInt(rs.getString("connection.threadpool"));

    private synchronized void initializeConnectionPool(){
        while(!checkIfConnectionPoolIsFull()){
            Connection newConnection = MySQLConnectionUtil.getConnection();
            connectionInUes.add(newConnection);
        }
        notifyAll();
    }

    private boolean checkIfConnectionPoolIsFull(){
        return connectionInUes.size() >= MAX_CONNECTION;
    }

    @Override
    public synchronized Connection getConnection() {
        while(connectionInUes.size() == 0 || connectionInUes.size() == MAX_CONNECTION){
            //wait for an existing connection to be freed up
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return connectionInUes.poll();
    }

    @Override
    public boolean releaseConnection(Connection connection) {
        try{
            if (connection.isClosed()){
                initializeConnectionPool();
            }else {
                boolean isReleased = connectionInUes.offer(connection);
                notifyAll();

                return isReleased;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return false;
    }
}
