package com.lecuong.entity;

import com.lecuong.common.annotation.Column;
import com.lecuong.common.annotation.Entity;
import com.lecuong.common.annotation.Id;

@Entity(name = "role")
public class Role {

    @Id(value = "id")
    private int id;

    @Column(value = "name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
