package com.lecuong.service;

import com.lecuong.exception.ObjectNotFoundException;
import com.lecuong.model.request.Auth;
import com.lecuong.model.request.UserRequest;
import com.lecuong.model.response.UserResponse;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public interface UserService {

    void insert(UserRequest userRequest) throws SQLException, NoSuchMethodException, IllegalAccessException, InvocationTargetException;

    void update(int id, UserRequest userRequest) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, SQLException;

    UserResponse findByUserNameAndPassword(Auth auth) throws SQLException, NoSuchFieldException, InstantiationException, IllegalAccessException, ObjectNotFoundException, NoSuchMethodException, InvocationTargetException;
}
