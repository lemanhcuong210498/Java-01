package com.lecuong.service.impl;

import com.lecuong.converter.UserConverter;
import com.lecuong.entity.User;
import com.lecuong.exception.ObjectNotFoundException;
import com.lecuong.model.request.Auth;
import com.lecuong.model.request.UserRequest;
import com.lecuong.model.response.UserResponse;
import com.lecuong.repository.UserRepository;
import com.lecuong.repository.impl.BaseQuery;
import com.lecuong.repository.impl.UserRepositoryImpl;
import com.lecuong.service.UserService;
import com.lecuong.utils.ObjectUlti;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(){
        userRepository = new UserRepositoryImpl();
    }

    //nhan vao 1 userRequest, chuyen doi userRequest -> userEntity
    @Override
    public void insert(UserRequest userRequest) throws SQLException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        User user = new User();
        ObjectUlti.copyProperties(userRequest, user);
        userRepository.insert(user);
    }

    @Override
    public void update(int id, UserRequest userRequest) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, SQLException {

        User user = new User();
        ObjectUlti.copyProperties(userRequest, user);
        user.setId(id);
        userRepository.update(user);
    }

    @Override
    public UserResponse findByUserNameAndPassword(Auth auth) throws SQLException, NoSuchFieldException, InstantiationException, IllegalAccessException, ObjectNotFoundException, NoSuchMethodException, InvocationTargetException {

        Optional<User> user = userRepository.findByUserNameAndPassword(auth.getUserName(), auth.getPassword());

        UserResponse userResponse = new UserResponse();
        user.orElseThrow(ObjectNotFoundException::new);

        ObjectUlti.copyProperties(user.get(), userResponse);

        return userResponse;
    }
}
