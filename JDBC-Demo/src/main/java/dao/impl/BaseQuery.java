package dao.impl;

import dao.JpaRepository;
import paging.Pageable;
import paging.impl.PageRequest;
import utils.AnnotationUtil;
import utils.MySQLConnectionUtil;
import utils.ObjectUlti;
import utils.pool.ConnectionPool;
import utils.pool.ConnectionPoolImpl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BaseQuery<T, ID> implements JpaRepository<T, ID> {

    private ConnectionPool connectionPool;
    private Class<T> tClass;
    private Connection connection;

    public BaseQuery() {
        connectionPool = new ConnectionPoolImpl();
        tClass = (Class<T>) ((ParameterizedType) (getClass().getGenericSuperclass())).getActualTypeArguments()[0];
        connection = connectionPool.getConnection();
    }

    @Override
    public <S extends T> S insert(T entity) throws SQLException {

        StringBuilder sql = new StringBuilder(Query.INSERT);
        sql.append(AnnotationUtil.getTableName(tClass)).append("(");
        Field[] fields = entity.getClass().getDeclaredFields();

        Arrays.stream(fields).forEach(field -> {
            try {
                if (field.getName().equals("id")) {
                    sql.append(AnnotationUtil.getPrimaryKey(tClass, field.getName())).append(",");
                } else {
                    sql.append(AnnotationUtil.getFieldName(tClass, field.getName())).append(",");
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        });

        sql.deleteCharAt(sql.length() - 1);
        sql.append(")").append(Query.VALUES).append("(");

        Arrays.stream(fields).forEach(field -> {
            sql.append("?").append(",");
        });

        sql.deleteCharAt(sql.length() - 1).append(")");
        connection.setAutoCommit(false);

        PreparedStatement ps = connection.prepareStatement(sql.toString());

        try {
            for (int i = 0; i < fields.length; i++) {
                ps.setObject(i + 1, ObjectUlti.getMethod(entity, fields[i]));
            }
            ps.executeUpdate();
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
        }
        return (S) entity;
    }

    @Override
    public void update(T entity) throws SQLException {

        StringBuilder sql = new StringBuilder(Query.UPDATE)
                .append(AnnotationUtil.getTableName(tClass))
                .append(Query.SET);

        Field[] fields = entity.getClass().getDeclaredFields();

        try {
            for (int i = 1; i < fields.length; i++) {
                Field field = fields[i];
                sql.append(AnnotationUtil.getFieldName(tClass, field.getName())).append(" = ?,");
            }

            sql.deleteCharAt(sql.length() - 1);
            sql.append(Query.WHERE).append(AnnotationUtil.getPrimaryKey(tClass, fields[0].getName())).append(" = ?");

            connection.setAutoCommit(false);

            PreparedStatement ps = connection.prepareStatement(sql.toString());

            for (int i = 1; i < fields.length; i++) {
                ps.setObject(i, ObjectUlti.getMethod(entity, fields[i]));
            }

            ps.setObject(fields.length, ObjectUlti.getMethod(entity, fields[0]));
            ps.executeUpdate();

            connection.commit();

        } catch (Exception e) {
            connection.rollback();
        }
    }

    @Override
    public void delete(ID id) throws NoSuchFieldException, SQLException {

        StringBuilder sql = new StringBuilder(Query.DELETE)
                .append(AnnotationUtil.getTableName(tClass))
                .append(Query.WHERE)
                .append(AnnotationUtil.getPrimaryKey(tClass, "id"))
                .append(" = ?");

        PreparedStatement ps = connection.prepareStatement(sql.toString());
        ps.setObject(1, id);

        ps.executeUpdate();
    }

    @Override
    public <S extends T> S findById(ID id) {

        try {
            StringBuilder sql = new StringBuilder(Query.SELECT)
                    .append(AnnotationUtil.getTableName(tClass))
                    .append(Query.WHERE)
                    .append(AnnotationUtil.getPrimaryKey(tClass, "id"))
                    .append(" = ?");

            PreparedStatement ps = connection.prepareStatement(sql.toString());
            ps.setObject(1, id);

            ResultSet rs = ps.executeQuery();
            Object object = null;
            while (rs.next()) {
                object = ObjectUlti.map(tClass, rs);
            }

            return (S) object;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public <S extends T> List<S> findAll() throws SQLException, IllegalAccessException, NoSuchFieldException, InstantiationException {

        StringBuilder sql = new StringBuilder(Query.SELECT)
                .append(AnnotationUtil.getTableName(tClass));

        PreparedStatement ps = connection.prepareStatement(sql.toString());

        ResultSet rs = ps.executeQuery();
        List<T> list = new ArrayList<>();
        while (rs.next()) {
            T t = (T) ObjectUlti.map(tClass, rs);

            list.add(t);
        }

        return (List<S>) list;
    }

    @Override
    public long count() throws SQLException {

        StringBuilder sql = new StringBuilder(Query.COUNT)
                .append(AnnotationUtil.getTableName(tClass));

        PreparedStatement ps = connection.prepareStatement(sql.toString());

        ResultSet rs = ps.executeQuery();
        long result = 0;
        while (rs.next()) {
            result = rs.getLong(1);
        }
        return result;
    }

    @Override
    public <S extends T> List<S> findAll(PageRequest pageable) throws SQLException, IllegalAccessException, NoSuchFieldException, InstantiationException {
        StringBuilder sql = new StringBuilder(Query.SELECT)
                .append(AnnotationUtil.getTableName(tClass))
                .append(Query.LIMIT)
                .append(pageable.getSize())
                .append(Query.OFFSET)
                .append(pageable.getOffset());

        PreparedStatement ps = connection.prepareStatement(sql.toString());

        ResultSet rs = ps.executeQuery();
        List<T> list = new ArrayList<>();
        while (rs.next()) {
            T t = (T) ObjectUlti.map(tClass, rs);

            list.add(t);
        }

        return (List<S>) list;
    }

    public interface Query {
        String SELECT = "SELECT * FROM ";
        String COUNT = "SELECT COUNT(*) FROM ";
        String WHERE = " WHERE ";
        String AND = " AND ";
        String OR = " OR ";
        String LIKE = " LIKE ";
        String INSERT = "INSERT INTO";
        String UPDATE = "UPDATE ";
        String DELETE = "DELETE FROM ";
        String SET = " SET ";
        String ORDER_BY = "ORDER BY";
        String VALUES = " VALUES ";
        String LIMIT = " LIMIT ";
        String OFFSET = " OFFSET ";
    }
}
